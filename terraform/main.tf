#===================================================PROVIDER========================================================

terraform {
	backend "http" {}
	required_providers {
	  aws = {
	    source	= "hashicorp/aws"
	  }
	}
}

provider "aws" {
	region		= "eu-central-1"
}

#==================================================RESOURCES========================================================

#---------------------------------------------------UBUNTU

resource "aws_instance" "test_srv" {
	ami		= "ami-065deacbcaac64cf2"
	instance_type	= "t3.micro"
	user_data = "sudo apt update & sudo apt install nginx & sudo systemctl start nginx"

	tags = {
	  Name		= "Ubuntu WEB"
	  Owner		= "Denis Kireev"
	  Project	= "Test task"
	}
}

#---------------------------------------------------Security-Group

resource "aws_security_group" "Test_Web" {
	name        = "TestSG"
	description = "SecurityGroup for Test task"

	ingress {
	  from_port	= 80
	  to_port	= 80
	  protocol	= "tcp"
	  cidr_blocks	=["0.0.0.0/0"]
	}

	egress {
	  from_port	= 0
	  to_port	= 0
	  protocol	= "-1"
	  cidr_blocks	= ["0.0.0.0/0"]
	}

	tags = {
	  Name = "TestSG"
	}
}

#------------------------------------Load-Balancer

resource "aws_elb" "Test" {
  availability_zones = ["eu-central-1b"]
  security_groups = [aws_security_group.Test_Web.id]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

#  listener {
#    instance_port      = 80
#    instance_protocol  = "http"
#    lb_port            = 443
#    lb_protocol        = "https"
#    ssl_certificate_id = "arn:aws:iam::123456789012:server-certificate/certName"
#  }

  instances                   = [aws_instance.test_srv.id]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
}
